INSTALLATION
============

RPMs
~~~~

Binary, debuginfo and source RPMs for the VDP are available at:

	https://pkg.uplex.de/

The packages are built for Enterprise Linux 7 (el7), and hence will
run on compatible distros (such as RHEL7, Fedora, CentOS 7 and Amazon
Linux).

To add the repo to your YUM configuration::

  yum-config-manager --add-repo https://pkg.uplex.de/rpm/7/uplex-varnish/x86_64/

The RPM for the VDP requires a Varnish installation from the official
packages:

	https://packagecloud.io/varnishcache

You can then install the VDP with::

  yum install --nogpgcheck vdp-pesi

If you have problems or questions concerning the RPMs, post an issue
to the source repository web site for the VDP, or contact
<varnish-support@uplex.de>.

Building from source
~~~~~~~~~~~~~~~~~~~~

The VDP is built against both a Varnish development installation *and*
a Varnish source tree. The Varnish source tree must be built, so that
generated sources are present; that is, its build process must be
carried out at least as far the execution of ``make``.  The build
version of installed Varnish must match the version of the source
tree; this means that the "commit ID" portion of the version string of
installed Varnish must match the HEAD of the source tree.  A simple
solution is to build and install Varnish from the source tree.  If
Varnish is installed by other means (for example from package), then
the commit IDs must match.

For the installation, the autotools use ``pkg-config(1)`` to locate
the necessary header files and other resources for Varnish. The root
directory of the source tree is identified by the variable
``VARNISHSRC`` in the invocation of ``configure``.

This sequence will install the VDP::

  > ./autogen.sh	# for builds from the git repo
  > ./configure VARNISHSRC=/path/to/source/tree
  > make
  > make check		# to run unit tests in src/tests/*.vtc
  > make distcheck	# run check and prepare a distribution tarball
  > sudo make install

See `CONTRIBUTING.rst <CONTRIBUTING.rst>`_ for notes about building
from source.

If you have installed Varnish in non-standard directories, call
``autogen.sh`` and ``configure`` with the ``PKG_CONFIG_PATH``
environment variable set to include the paths where the ``.pc`` file
can be located for ``varnishapi``.  For example, when varnishd
configure was called with ``--prefix=$PREFIX``, use::

  > PKG_CONFIG_PATH=${PREFIX}/lib/pkgconfig
  > export PKG_CONFIG_PATH

By default, the ``configure`` script installs the VDP in the VMOD
directory for Varnish, determined via ``pkg-config(1)``. The VMOD
installation directory can be overridden by passing the ``VMOD_DIR``
variable to ``configure``.

Other files such as the man-page are installed in the locations
determined by ``configure``, which inherits its default ``--prefix``
setting from Varnish.
