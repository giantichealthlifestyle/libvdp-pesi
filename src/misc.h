#define OC_F_FINAL (OC_F_PRIVATE | OC_F_HFM | OC_F_HFP)

void req_fini(struct req **, struct worker *);

#define VFAIL(ctx, fmt, ...)					\
	VRT_fail((ctx), "vdp pesi failure: " fmt, __VA_ARGS__)
