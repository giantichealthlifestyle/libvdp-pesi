extern int block_final, front_push;

#define PF_HAS_TASK		1U
/* vcl-controlled flags */
#define PF_CFG_SERIAL		(1U<<1)
#define PF_CFG_THREAD		(1U<<2)
/* undocumented for now */
#define PF_CFG_BLOCK_FINAL	(1U<<3)
#define PF_CFG_FRONT_PUSH	(1U<<4)

#define PF_CFG_DEFAULT					\
	( PF_CFG_THREAD				\
	| (block_final ? PF_CFG_BLOCK_FINAL : 0)	\
	| (front_push ? PF_CFG_FRONT_PUSH : 0)		\
	)

#define PF_MASK_CFG		\
	( PF_CFG_SERIAL	\
	| PF_CFG_THREAD	\
	| PF_CFG_BLOCK_FINAL	\
	| PF_CFG_FRONT_PUSH	\
	)
