varnishtest "ESI include with http://"


server s1 {
	rxreq
	expect req.url == "/foo/bar"
	txresp -body {
		<html>
		Before include
		<!--esi <esi:include src="http://bozz/body"/> -->
		After include
	}
} -start


server s2 {
	rxreq
	expect req.url == "/body"
	txresp -body {
		<pre>Included file</pre>
	}
} -start

varnish v1 -vcl+backend {
	import ${vmod_pesi};
	import ${vmod_pesi_debug};
	include "debug.inc.vcl";

	sub vcl_backend_fetch {
		if (bereq.http.host == "bozz") {
			set bereq.backend = s2;
		} else {
			set bereq.backend = s1;
		}
	}
	sub vcl_backend_response {
		set beresp.do_esi = true;
	}

	sub vcl_deliver {
		pesi.activate();
	}
} -start

client c1 {
	txreq -url /foo/bar -hdr "Host: froboz"
	rxresp
	expect resp.status == 200
	expect resp.bodylen == 78
	expect resp.body == {
		<html>
		Before include
		 
		<pre>Included file</pre>
	 
		After include
	}
}

client c1 -run
varnish v1 -expect esi_errors == 0
varnish v1 -expect MAIN.s_resp_bodybytes == 78

# Now try with invalid URLs

server s1 {
	rxreq
	expect req.url == /http
	txresp -body {<esi:include src="http://foobar" />1234}
	rxreq
	expect req.url == /https
	txresp -body {<esi:include src="https://foobar" />123456}
} -start

varnish v1 -vcl+backend {
	sub vcl_recv {
		set req.backend_hint = s2;
		set req.backend_hint = s1;
	}
	sub vcl_backend_response {
		set beresp.do_esi = true;
	}
}

varnish v1 -cliok "param.set feature +esi_ignore_https"

logexpect l1 -v v1 -g raw {
	expect * * ESI_xmlerror "ERR after 0 ESI 1.0 <esi:include> invalid src= URL"
	expect * * ESI_xmlerror "WARN after 0 ESI 1.0 <esi:include> https:// treated as http://"
	expect * * ESI_xmlerror "ERR after 0 ESI 1.0 <esi:include> invalid src= URL"
} -start

client c1 {
	txreq -url /http
	rxresp
	expect resp.status == 200
	expect resp.bodylen == 4
	expect resp.body == "1234"
} -run

varnish v1 -expect esi_errors == 1
varnish v1 -expect MAIN.s_resp_bodybytes == 82

client c1 {
	txreq -url /https
	rxresp
	expect resp.status == 200
	expect resp.bodylen == 6
	expect resp.body == "123456"
} -run

logexpect l1 -wait

varnish v1 -expect esi_errors == 2
varnish v1 -expect MAIN.s_resp_bodybytes == 88

## HTTP/2

varnish v1 -cliok "param.set feature +http2"

client c1 {
	stream 1 {
		txreq -url /foo/bar -hdr host froboz
		rxresp
		expect resp.status == 200
		expect resp.bodylen == 78
		expect resp.body == {
		<html>
		Before include
		 
		<pre>Included file</pre>
	 
		After include
	}
	} -run
	delay .1
	stream 3 {
		txreq -url /http
		rxresp
		expect resp.status == 200
		expect resp.bodylen == 4
		expect resp.body == "1234"
	} -run
	delay .1
	stream 5 {
		txreq -url /https
		rxresp
		expect resp.status == 200
		expect resp.bodylen == 6
		expect resp.body == "123456"
	} -run
} -run
